// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    // interface PageData {}
    // interface PageState {}
    // interface Platform {}
  }

  // Object object upgrade
  interface ObjectConstructor {
    keys<T>(obj: T): (keyof T)[];
    // entries<T>(obj: T): T extends Record<infer P, infer R> ? [P, R][] : [string, unknown][];
    entries<T, P>(obj: Record<T, P>): [T, P][];
  }

  declare module "*?filesize" {
    const filesize: number;
    export default filesize;
  }
}

export {};
