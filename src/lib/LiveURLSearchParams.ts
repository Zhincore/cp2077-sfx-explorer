import { get, writable, type Writable } from "svelte/store";
import { pushState, replaceState } from "$app/navigation";

export class LiveURLSearchParams {
  #store: Writable<URLSearchParams>;
  #lastSuffix = "";

  constructor(init?: string | URLSearchParams, replace = true) {
    this.#store = writable(new URLSearchParams(init));

    this.#store.subscribe((params) => {
      const suffix = "?" + params;
      if (suffix === this.#lastSuffix) return;
      const lastSuffix = this.#lastSuffix;
      this.#lastSuffix = suffix;

      if (!lastSuffix) return; // ignore initial update

      const url = new URL(window?.location.href);
      url.search = suffix == "?" ? "" : suffix;

      if (replace) replaceState(url, {});
      else pushState(url, {});
    });
  }

  get<Default = null>(name: string, defaultValue?: Default) {
    return get(this.#store).get(name) ?? defaultValue ?? (null as Default);
  }

  set(name: string, value: string) {
    this.#store.update((params) => {
      params.set(name, value);
      return params;
    });
  }

  delete(name: string) {
    this.#store.update((params) => {
      params.delete(name);
      return params;
    });
  }
}
