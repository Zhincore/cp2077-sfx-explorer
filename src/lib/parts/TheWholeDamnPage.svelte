<script
  lang="ts"
  generics="RawData extends Record<string|number, any>, ProcessedDataItem extends Record<string|number, any>"
>
  /* eslint-disable no-undef */
  import type { FuseOptionKey } from "fuse.js";
  import { onDestroy, onMount } from "svelte";
  import { fade } from "svelte/transition";
  import { LoaderIcon } from "svelte-feather-icons";
  import { twMerge } from "tailwind-merge";
  import { browser } from "$app/environment";
  import { goto } from "$app/navigation";
  import { page } from "$app/stores";
  import VERSION from "$content/version.json";
  import { PUBLIC_PREVIEW_URL } from "$env/static/public";
  import Alert from "$lib/components/Alert.svelte";
  import Arrow from "$lib/components/Arrow.svelte";
  import Button from "$lib/components/Button.svelte";
  import Dialog from "$lib/components/Dialog.svelte";
  import LoadingBar from "$lib/components/LoadingBar.svelte";
  import Pager from "$lib/components/Pager.svelte";
  import PanelBackground from "$lib/components/PanelBackground.svelte";
  import WindowedGrid from "$lib/components/WindowedGrid.svelte";
  import { download } from "$lib/download";
  import { isOffline } from "$lib/isOfflineStore";
  import { localStore } from "$lib/localStore";
  import { isNull } from "$lib/utils";
  import { SoundState, useVisualizedAudio } from "$lib/visualizedAudio";
  import ItemFilter from "./ItemFilter.svelte";
  import type { FilterOption } from "./ItemFilterOption.svelte";

  type ItemId = string | number | null | undefined;

  export let itemWidth: number;
  export let itemHeight: number;
  export let file: string;
  export let fileSize: number;
  export let convertId: (id: string) => ItemId = parseInt;
  export let getItemId: (item: ProcessedDataItem) => ItemId;
  export let getPreviewUrl: (id: NonNullable<ItemId>) => string;
  export let processData: (data: RawData) => ProcessedDataItem[];
  export let keys: FuseOptionKey<ProcessedDataItem>[];
  export let filters: Record<string, FilterOption<ProcessedDataItem>>;
  export let searchHint = "";
  export let dataName = "SFX map";
  export let contentName = "sounds";
  export let detailName: string | ((item: ProcessedDataItem) => string) = "Sound Effect";
  export let gridClass = "";
  export let initialData: RawData | undefined = undefined;

  export let windowHeight = 1;
  export let innerWidth = 1;
  export let scrollY = 0;

  let loadingProgress = 0;
  export let data: ProcessedDataItem[] | undefined = undefined;
  let error: Error | undefined = undefined;
  let previewsAvailable: boolean | null = null;

  let playingId: ItemId = null;
  const audio = useVisualizedAudio();
  const { soundState, stopSound } = audio;

  let filterInitialized = false;
  let filteredData: ProcessedDataItem[] = [];
  let filterRunning = false;

  function checkStatic() {
    previewsAvailable = null;
    fetch(PUBLIC_PREVIEW_URL)
      .then(() => (previewsAvailable = true))
      .catch(() => (previewsAvailable = false));
  }

  async function runDownload() {
    data = undefined;
    error = undefined;

    try {
      const rawData = await download<RawData>(file, fileSize, (p) => (loadingProgress = p));
      data = processData(rawData);
    } catch (err) {
      error = err as Error;
    }
  }

  function playSound(id: NonNullable<ItemId>) {
    if (id == playingId) return stopSound();

    playingId = id;
    audio.playSound(getPreviewUrl(id));
    audio.seek(0);
  }

  function onDialogClosing() {
    if (!showActiveItem) return;

    if (filterInitialized) history.back();
    else goto(".");
  }

  if (initialData) data = processData(initialData);
  else onMount(runDownload);

  onMount(() => {
    document.scrollingElement?.classList.toggle("!scroll-auto", true);

    return () => {
      document.scrollingElement?.classList.toggle("!scroll-auto", false);
    };
  });

  const infiniteScroll = localStore("infiniteScroll-" + dataName, false);
  let pageIndex = 1;
  let pageCount = 1;

  $: getDetailName = (data: ProcessedDataItem[], activeIndex: number) =>
    typeof detailName === "function" ? detailName(data[activeIndex]) : detailName;

  $: if ($soundState == SoundState.STOPPED) playingId = null;

  let activeId: string | undefined;
  $: activeId = $page.params.id;
  $: showActiveItem = !isNull(activeId);

  let shownActiveId: ItemId = null;
  $: if (!isNull(activeId)) shownActiveId = convertId(activeId);
  $: activeIndex = !isNull(shownActiveId) ? data?.findIndex((v) => getItemId(v) == shownActiveId) : null;

  let focusedId: ItemId;
  $: if (filteredData) focusedId = null; // Reset focus on search
  $: if (!isNull(shownActiveId)) focusedId = shownActiveId;
  $: focusedIndex = focusedId ? filteredData.findIndex((v) => getItemId(v) == focusedId) : null;

  // Only load filter once needed
  $: if (!showActiveItem) filterInitialized = true;

  $: filteredLength = filteredData.length;
  $: if ($isOffline) previewsAvailable = false;
  else if (browser) checkStatic();

  $: browser && document.scrollingElement?.classList.toggle("overflow-hidden", !!activeId);

  onDestroy(() => {
    if (browser) document.scrollingElement?.classList.remove("overflow-hidden");
  });
</script>

<div
  data-sveltekit-noscroll
  class={twMerge(
    "relative flex flex-grow flex-col px-2 transition duration-500",
    showActiveItem && data && "pointer-events-none opacity-0",
  )}
>
  {#if !data}
    <div
      class="absolute inset-0 z-30 flex h-full max-h-screen w-full flex-grow flex-col items-center justify-center gap-8 bg-black p-4 pb-16"
      out:fade
    >
      <noscript>
        <Alert>
          <span slot="title">JavaScript is disabled!</span>
          <p>This application requires JavaScript to operate. Please enable JavaScript and try again.</p>
        </Alert>
      </noscript>

      {#if error}
        <Alert>
          <span slot="title">Downloading {dataName} failed!</span>
          <div class="flex flex-col">
            <p>Something went wrong when downloading the {dataName}. Please try again later.</p>

            <p class="my-2 font-mono text-sm text-yellow">{error.name}: {error.message}</p>

            <Button variant="small" class="ml-auto mt-4" on:click={runDownload}>Try again</Button>
          </div>
        </Alert>
      {:else if browser}
        <LoadingBar label="Loading {dataName}..." indeterminate={loadingProgress === 1} progress={loadingProgress} />
      {/if}
    </div>
  {:else if browser}
    {#if filterInitialized}
      <ItemFilter
        {searchHint}
        {data}
        {keys}
        {filters}
        {contentName}
        bind:filtered={filteredData}
        bind:processing={filterRunning}
        bind:infiniteScroll={$infiniteScroll}
      />
    {/if}

    <div class="mx-auto -mb-4 -mt-1 pt-[2px]" class:invisible={!pageCount}>
      <Pager bind:page={pageIndex} pages={pageCount} />
    </div>

    <div class="mx-auto flex min-h-12 w-full max-w-screen-xl flex-wrap items-start gap-x-2 px-2 max-md:mt-5">
      <div class="mr-auto pb-px text-red-dark">
        Last update: <span class="text-yellow">{VERSION.lastUpdate}</span>
      </div>

      <div class="w-1/2" />

      <div class="ml-auto">
        {#if previewsAvailable == null}
          <span class="text-red">
            <LoaderIcon class="mr-1 inline animate-spin align-top" />
            Checking previews availability...
          </span>
        {:else if previewsAvailable}
          <span class="text-cyan">Click on an item for details. Previews are low quality.</span>
        {:else}
          <div class="flex">
            <Alert inline>
              <span slot="title">Previews are unavailable.</span>
              {#if $isOffline}You are offline.{:else}The server might be down.{/if}
            </Alert>
            {#if !$isOffline}
              <Button on:click={checkStatic} wideStripe={false} class="-ml-[2px]">Retry</Button>
            {/if}
          </div>
        {/if}
      </div>
    </div>

    {#if !filterRunning && filteredLength == 0}
      <Alert class="mx-auto my-16 inline-flex">
        <span slot="title">No items matching the filter were found.</span>
        Try using less specific searching parameters.
      </Alert>
    {/if}

    {#if filterRunning}
      <div
        transition:fade
        class="absolute left-1/2 top-[40svh] z-40 -translate-x-1/2 text-center text-4xl font-bold uppercase text-red transition"
      >
        <LoaderIcon class="mx-auto animate-spin" size="2x" />
        Processing
      </div>
    {/if}

    <WindowedGrid
      {focusedIndex}
      pagination={!$infiniteScroll}
      itemCount={filteredLength}
      {itemWidth}
      {itemHeight}
      class={twMerge(gridClass, filterRunning ? "opacity-50" : "")}
      bind:windowHeight
      bind:wrapperWidth={innerWidth}
      bind:scrollY
      bind:page={pageIndex}
      bind:pageCount
      let:index
    >
      {#if index < filteredLength}
        {@const item = filteredData[index]}
        {@const id = getItemId(item)}
        {@const focused = !showActiveItem && focusedId == id}
        {@const onFocus = () => (focusedId = id)}
        {@const onBlur = () => focused && (focusedId = null)}
        <slot
          name="item"
          {focused}
          {item}
          {index}
          {playSound}
          {playingId}
          {previewsAvailable}
          {audio}
          {onFocus}
          {onBlur}
        />
      {/if}
    </WindowedGrid>

    {#if $infiniteScroll && scrollY > windowHeight * 0.5}
      <button
        transition:fade
        class="fixed left-1/2 top-28 flex h-12 w-12 -translate-x-1/2 text-cyan shadow-xl transition hover:bg-opacity-100 md:top-20"
        title="To the top"
        on:click={() => window.scrollTo({ top: 0, behavior: "smooth" })}
      >
        <PanelBackground class="-right-1 bg-yellow-dark bg-opacity-50 text-yellow" />

        <span class="sr-only">To the top</span>
        <Arrow class="m-auto rotate-90 p-2" />
      </button>
    {/if}

    <div class="mx-auto my-4" class:invisible={!pageCount}>
      <Pager bind:page={pageIndex} pages={pageCount} />
    </div>
  {/if}
</div>

<!-- Active item dialog -->
{#if data && !isNull(shownActiveId)}
  {@const exists = !isNull(activeIndex) && activeIndex >= 0}
  {@const defoActiveIndex = activeIndex ?? 0}
  <Dialog
    modal={false}
    show={showActiveItem}
    on:close={() => (shownActiveId = null)}
    on:closing={onDialogClosing}
    title={exists ? getDetailName(data, defoActiveIndex) : ""}
    class="max-h-[calc(100%-5rem)]"
  >
    {#if exists}
      <slot name="detail" {audio} item={data[defoActiveIndex]} />
    {:else}
      <Alert>
        <span slot="title">Item not found</span>
        <div class="flex flex-col">
          Item with ID {shownActiveId} was not found, make sure the URL is correct.
        </div>
      </Alert>
    {/if}
  </Dialog>
{/if}
