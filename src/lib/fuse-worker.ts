import Fuse from "fuse.js";

let fuse: Fuse<unknown> | undefined = undefined;

onmessage = function receiver({ data }) {
  switch (data.event) {
    case "initialize":
      fuse = new Fuse(data.data, data.options);
      postMessage({ event: "initialized" });
      break;

    case "update":
      if (!fuse) return;
      fuse.setCollection(data.data);
      postMessage({ event: "updated" });
      break;

    case "search":
      if (!fuse) return;

      try {
        const result = fuse.search(data.query).map((v) => v.refIndex);
        postMessage({ event: "searched", query: data.query, result, token: data.token });
      } catch (err) {
        console.error(err);
      }
      break;

    default:
      throw new Error("Received unexpected message!");
  }
};
