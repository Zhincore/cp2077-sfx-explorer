import EventEmitter from "eventemitter3";
import type { Expression, IFuseOptions } from "fuse.js";

interface OffthreadFuseEvents<T> {
  initialized(): void;
  searched(query: string, result: T[]): void;
  searchStarted(): void;
  updated(): void;
}

export class OffthreadFuse<T> extends EventEmitter<OffthreadFuseEvents<T>> {
  #worker = new Worker(new URL("./fuse-worker.ts", import.meta.url), { type: "module" });
  #timeout: NodeJS.Timeout | undefined = undefined;
  #pendingData: T[] | null = null;
  #data: T[] = [];
  #searchToken = 0;

  constructor(readonly debounce = 500) {
    super();
    this.#worker.addEventListener("message", ({ data: { event, ...data } }) => {
      switch (event) {
        case "initialized":
          this.emit(event);
          break;
        case "searched":
          if (data.token !== this.#searchToken) break;
          this.emit(
            event,
            data.query,
            data.result.map((i: number) => this.#data[i]),
          );
          break;
        case "updated":
          if (this.#pendingData) this.#data = this.#pendingData;
          this.#pendingData = null;

          this.emit("updated");
      }
    });
  }

  async initialize(data: T[], options: IFuseOptions<T> = {}) {
    this.#worker.postMessage({ event: "initialize", data, options });

    return new Promise<void>((r) => this.once("initialized", r));
  }

  destroy() {
    if (this.#timeout) clearTimeout(this.#timeout);
    this.#worker.terminate();
  }

  update(data: T[]) {
    this.#pendingData = data;
    this.#worker.postMessage({ event: "update", data });
  }

  scheduleSearch(query: string | Expression) {
    if (this.#timeout) clearTimeout(this.#timeout);

    this.#timeout = setTimeout(() => {
      this.#timeout = undefined;

      if (query) {
        const token = Math.random();
        this.#searchToken = token;
        this.emit("searchStarted");
        this.#worker.postMessage({ event: "search", query, token });
      }
    }, this.debounce);
  }
}
