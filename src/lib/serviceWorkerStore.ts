import { readable } from "svelte/store";
import { browser } from "$app/environment";

export const serviceWorkerReady = readable(false, (set) => {
  if (!browser || !("serviceWorker" in navigator)) return;

  const registerHandler = () => navigator.serviceWorker.ready.then(() => set(true));
  registerHandler();

  navigator.serviceWorker.addEventListener("controllerchange", registerHandler);
});
