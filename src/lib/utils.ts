export function isNull(value: unknown): value is null | undefined {
  return value === null || value === undefined;
}

export function defoNotNull<T>(value: T): NonNullable<T> {
  if (isNull(value)) throw new TypeError("defoNotNull was passed " + value);
  return value as NonNullable<T>;
}

export function isIn(
  value: string | number | symbol,
  object: Record<string | number | symbol, unknown>,
): value is keyof object {
  return value in object;
}
