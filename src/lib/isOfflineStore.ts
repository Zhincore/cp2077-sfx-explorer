import { readable } from "svelte/store";
import { browser } from "$app/environment";

export const isOffline = readable(browser && !navigator.onLine, (set) => {
  if (!browser) return;
  window.addEventListener("offline", () => set(true));
  window.addEventListener("online", () => set(false));
});
