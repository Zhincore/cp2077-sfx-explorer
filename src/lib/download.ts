// Source: https://stackoverflow.com/questions/47285198/fetch-api-download-progress-indicator
export async function* streamToAsyncIterable(stream: ReadableStream<Uint8Array>) {
  const reader = stream.getReader();
  try {
    while (true) {
      const { done, value } = await reader.read();
      if (done) return;
      yield value;
    }
  } finally {
    reader.releaseLock();
  }
}

const cache = new Map<string, unknown>();

export async function download<T = unknown>(file: string, size: number, callback: (progress: number) => void) {
  if (cache.has(file)) return cache.get(file) as T;

  const response = await fetch(file);

  if (!response.body) throw new Error("Download response has no body!");
  if (!response.ok) throw new Error(`Request failed with status ${response.status}: ${response.statusText}`);

  let responseSize = 0;
  const chunks: Uint8Array[] = [];

  for await (const chunk of streamToAsyncIterable(response.body)) {
    responseSize += chunk.length;
    chunks.push(chunk);
    callback(Math.min(1, responseSize / size));
  }

  const bytes = new Uint8Array(responseSize);
  let offset = 0;
  for (const chunk of chunks) {
    bytes.set(chunk, offset);
    offset += chunk.length;
  }

  callback(1);

  // Give UI a moment to breathe and show progress
  return new Promise<T>((resolve, reject) => {
    setTimeout(() => {
      try {
        const result = JSON.parse(new TextDecoder().decode(bytes));
        resolve(result);
        cache.set(file, result);
      } catch (err) {
        reject(err as Error);
      }
    }, 10);
  });
}
