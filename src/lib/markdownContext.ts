import { getContext, setContext } from "svelte";
import { writable, type Writable } from "svelte/store";

const key = Symbol("Markdown Context");

export interface HeadingRecord {
  title: string;
  id: string;
  children: HeadingRecord[];
}
export interface MarkdownContext {
  headings: Writable<HeadingRecord[]>;
  addHeading: (depth: number, heading: Omit<HeadingRecord, "children">) => void;
}

export const createMarkdownContext = () => {
  const initial: HeadingRecord[] = [];
  const headings = writable<HeadingRecord[]>(initial);
  const stack: HeadingRecord[][] = [initial];

  return setContext<MarkdownContext>(key, {
    headings,
    addHeading(depth, heading) {
      const obj = { ...heading, children: [] };

      // Go back to desired level
      while (depth < stack.length) stack.pop();

      stack[depth - 1].push(obj);
      stack.push(obj.children);
      headings.set(stack[0]);
    },
  });
};

export const getMarkdownContext = () => getContext<MarkdownContext>(key);
