import { onDestroy } from "svelte";
import { get, readonly, writable, type Readable } from "svelte/store";
import { browser } from "$app/environment";

export enum SoundState {
  STOPPED,
  PLAYING,
  LOADING,
  ERROR,
}

export interface VisualizerAudioCntr {
  soundState: Readable<SoundState>;
  soundProgress: Readable<number>;
  analyserData: Readable<Uint8Array>;
  currentSrc: Readable<string>;
  audioDuration: Readable<number>;
  stopSound: () => void;
  loadSound: (src: string) => void;
  playSound: (src?: string) => void;
  seek: (time: number) => void;
  setFFTSize: (size?: number) => void;
}

export function useVisualizedAudio(fftSize = 64): VisualizerAudioCntr {
  let _fftSize = fftSize;
  const soundState = writable(SoundState.STOPPED);
  const soundProgress = writable(0);
  const analyserData = writable<Uint8Array>(new Uint8Array());
  const currentSrc = writable("");
  const audioDuration = writable(0);

  let audio: HTMLAudioElement;
  if (browser) {
    audio = new Audio();
    audio.onended = () => {
      soundProgress.set(1);
      analyserData.update((v) => v.fill(127));
      stopSound();
    };
    audio.crossOrigin = "anonymous";
  }

  currentSrc.subscribe((v) => audio && (audio.src = v));

  let audioCtx: AudioContext | undefined = undefined;
  let analyser: AnalyserNode | undefined = undefined;
  let animationFrame = 0;

  function stopSound() {
    soundState.set(SoundState.STOPPED);
    audio?.pause();
  }

  function createAnalyserData() {
    const _analyserData = new Uint8Array(analyser!.frequencyBinCount);
    _analyserData.fill(127);
    analyserData.set(_analyserData);
  }

  function loadSound(src: string) {
    stopSound();
    soundProgress.set(0);

    currentSrc.set(src);
  }

  function playSound(src?: string) {
    if (!audioCtx || !analyser) {
      audioCtx = new AudioContext();
      analyser = audioCtx.createAnalyser();
      analyser.fftSize = _fftSize;

      audioCtx.createMediaElementSource(audio).connect(analyser);
      analyser.connect(audioCtx.destination);

      createAnalyserData();
    }

    stopSound();

    if (src && audio.src != src) {
      loadSound(src);
    }

    soundState.set(SoundState.LOADING);
    audio
      .play()
      .then(() => {
        soundState.set(SoundState.PLAYING);
        audioDuration.set(audio.duration);
        updateVisualizer();
      })
      .catch(() => {
        createAnalyserData(); // Clear
        soundState.set(SoundState.ERROR);
      });
  }

  function updateProgress() {
    soundProgress.set(audio.currentTime / audio.duration);
  }

  function updateVisualizer() {
    if (!audio || !analyserData || get(soundState) != SoundState.PLAYING) return;

    animationFrame = window.requestAnimationFrame(updateVisualizer);

    updateProgress();

    const _analyserData = get(analyserData);
    analyser?.getByteTimeDomainData(_analyserData);
    analyserData.set(_analyserData);
  }

  onDestroy(() => {
    stopSound();
    if (animationFrame) window.cancelAnimationFrame(animationFrame);
  });

  return {
    soundState: readonly(soundState),
    soundProgress: readonly(soundProgress),
    analyserData: readonly(analyserData),
    currentSrc: readonly(currentSrc),
    audioDuration: readonly(audioDuration),
    stopSound,
    loadSound,
    playSound,
    seek: (time) => {
      audio.currentTime = time * (audio.duration || 0);
      updateProgress();
    },
    setFFTSize: (size) => {
      _fftSize = size ?? fftSize;

      if (analyser) {
        analyser.fftSize = _fftSize;
        createAnalyserData();
      }
    },
  };
}
