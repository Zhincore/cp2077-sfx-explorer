import { writable } from "svelte/store";
import { browser } from "$app/environment";

export function localStore<T>(name: string, initial: T) {
  const saved = browser && localStorage.getItem(name);
  const store = writable(saved ? JSON.parse(saved) : initial);

  store.subscribe((value) => browser && localStorage.setItem(name, JSON.stringify(value)));

  return store;
}
