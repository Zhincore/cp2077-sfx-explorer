export const DLCS = { base: "base game", ep1: "Phantom Liberty" };

export const GENDER_COLORS = {
  female: "text-female",
  male: "text-male",
};
