import type { PageLoad } from "./$types";

export const prerender = "auto";

export const load = (({ params, data }) => {
  return {
    ...data,
    title: params.id ? "Subtitle Entry " + params.id : "Voice-over & Subtitles",
    description: params.id ? false : "List of all voice-overs and their subtitles in Cyberpunk 2077.",
  };
}) satisfies PageLoad;
