import { isbotNaive } from "isbot";
import path from "$data/subtitles.json?url";
import { ITEM_SERVER_RENDER } from "$env/static/private";
import type { PageServerLoad } from "./$types";
import type { Subtitles } from "./subtitles";

export const load = (async ({ isDataRequest, request, fetch }) => {
  if (ITEM_SERVER_RENDER !== "true") return;
  if (isDataRequest || !isbotNaive(request.headers.get("user-agent"))) return;

  // For server render load the data
  return { subtitles: await fetch(path).then((r) => r.json() as Promise<Subtitles>) };
}) satisfies PageServerLoad;
