import { PUBLIC_PREVIEW_URL, PUBLIC_PREVIEW_URL_SUFFIX } from "$env/static/public";

export interface SubtitlesSubitem {
  text?: string;
  vo?: Partial<Record<"main" | "holocall" | "helmet" | "reversed", string>>;
}

export interface SubtitlesItem {
  female?: SubtitlesSubitem;
  male?: SubtitlesSubitem;
  _path?: string;
}

export type Subtitles = Record<string, SubtitlesItem>;

/** Subtitles preprocessed for searching */
export interface ProcessedSubtitle extends SubtitlesItem {
  _id: string;
  _dlc: string;
  _character?: string;
  _v_gender: { m: boolean; f: boolean };
  _texts: string[];
  _pathSegments: string[];
}

const NAMES: Record<string, string> = {
  civ: "NPC",
  ncpd: "NCPD",
  t: "T-Bug",
  gang: "gang member",
  sobchak: "River Ward",
  bella: "Aurore Cassell",
};

export function getFullPath(path: string) {
  return path.replace("{}", "localization/en-us");
}

export function getVoiceoverPreviewUrl(path: string): string {
  return PUBLIC_PREVIEW_URL + "vo/" + getFullPath(path).replace(/\.wem$/, PUBLIC_PREVIEW_URL_SUFFIX);
}

export function guessCharacter(item: SubtitlesItem) {
  const vo = item.female?.vo ?? item.male?.vo;
  if (!vo) return;

  const path = Object.values(vo)[0].split("/");
  const filename = path[path.length - 1].split("_");

  const prefix = filename[0];

  // If alias is defined, return it
  if (prefix in NAMES) return NAMES[prefix];

  // Otherwise just capitalize it
  return prefix[0].toUpperCase() + prefix.slice(1);
}

const LANG_NODE_RE = /^<(?:mothertongue|kiroshi) (.+)\/>$/iu;
const LANG_ATTR_RE = /\b(\w)="(.*?)(?<!\\)"/giu;
const LETTER_TO_ATTR: Record<string, keyof LanguageNode> = {
  l: "lang",
  m: "content",
  o: "content",
  a: "after",
  b: "before",
  t: "translation",
};

export function parseSubtitle(input: string) {
  LANG_NODE_RE.lastIndex = 0;
  const nodeMatch = LANG_NODE_RE.exec(input);
  if (!nodeMatch) return input;

  const node = {} as LanguageNode;

  let match: RegExpExecArray | null;
  LANG_ATTR_RE.lastIndex = 0;
  while ((match = LANG_ATTR_RE.exec(nodeMatch[1]!)) !== null) {
    const [_, letter, content] = match;
    node[LETTER_TO_ATTR[letter]] = content;
  }

  return node;
}

export interface LanguageNode {
  lang: string;
  content: string;
  translation?: string; // Only Kiroshi node
  before: string;
  after: string;
}
