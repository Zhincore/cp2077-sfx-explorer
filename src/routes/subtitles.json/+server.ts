import { redirect } from '@sveltejs/kit';
import path from "$data/subtitles.json?url"
import type { RequestHandler } from './$types';

export const prerender = true;

export const GET: RequestHandler = async () => {
    redirect(302, path);
};
