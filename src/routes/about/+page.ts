import type { PageLoad } from "./$types";

export const ssr = true;

export const load = (() => {
  return {
    title: "About",
  };
}) satisfies PageLoad;
