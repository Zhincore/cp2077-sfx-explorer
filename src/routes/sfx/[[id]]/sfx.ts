import { PUBLIC_PREVIEW_URL, PUBLIC_PREVIEW_URL_SUFFIX } from "$env/static/public";

export interface SFXEntry {
  hash: number;
  location: "wem" | "opuspak" | "bnk" | "virtual";
  isMusic: boolean;
  pak?: string;
  embeddedIn?: string[];
  events: string[];
  tags: string[];
  gameParameter?: string[];
  state?: string[];
  stateGroup?: string[];
  switch?: string[];
  switchGroup?: string[];
}

export type SFXMap = Record<string, SFXEntry>;

export function getSFXPreviewUrl(id?: number): string {
  return PUBLIC_PREVIEW_URL + "sfx/" + (id ? id + PUBLIC_PREVIEW_URL_SUFFIX : "");
}
