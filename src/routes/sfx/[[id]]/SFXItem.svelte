<script lang="ts">
  import { FileIcon, PackageIcon } from "svelte-feather-icons";
  import { twMerge } from "tailwind-merge";
  import AudioButton from "$lib/components/AudioButton.svelte";
  import CopyButton from "$lib/components/CopyButton.svelte";
  import WrappableText from "$lib/components/WrappableText.svelte";
  import type { VisualizerAudioCntr } from "$lib/visualizedAudio";
  import { type SFXEntry } from "./sfx";

  export let index = 0;
  export let item: SFXEntry;
  export let focus = false;
  export let playingId: number | null = 0;
  export let previewsAvailable: null | boolean = null;
  export let audio: VisualizerAudioCntr;
  export let playSound: (hash: number) => void;

  const { analyserData, soundProgress, soundState } = audio;

  let self: HTMLElement;

  $: thisPlaying = playingId === item.hash;
  $: exists = item.location != "virtual";
  $: if (focus) self?.focus();
</script>

<a
  bind:this={self}
  href="/sfx/{item.hash}"
  tabIndex={index + 1}
  on:focus
  on:blur
  class={twMerge(
    "group relative m-3 grid h-[calc(100%-1.5rem)] w-[calc(100%-1.5rem)] cursor-pointer grid-cols-[auto_1fr] grid-rows-[auto_1fr] gap-x-2 p-2 outline-none",
    "border-2 border-red-darker bg-red-darkest",
    "transition duration-300 hover-focus:border-red",
    "focus:bg-[color-mix(in_srgb,theme(colors.red.darker)_30%,theme(colors.red.darkest))]",
  )}
>
  <!-- Preview button -->
  <!-- svelte-ignore a11y-click-events-have-key-events -->
  <!-- svelte-ignore a11y-no-static-element-interactions -->
  <div
    class={twMerge(
      "relative -ml-4 -mt-4 border-inherit bg-inherit p-1 pb-1 pl-2.5 text-center transition",
      "before:corner-bevel before:-right-2 before:top-0 before:rotate-45 before:border-inherit before:wh-[14px]",
      "after:corner-bevel after:-bottom-2 after:left-0 after:rotate-[225deg] after:border-inherit after:wh-[14px]",
    )}
    on:click|stopPropagation|preventDefault
  >
    <div class="absolute -inset-[2px] -z-20 border-2 border-inherit"></div>

    <div class="text-[8px] uppercase text-zinc-300 text-opacity-50">
      {#if item.location == "virtual"}
        <span class="text-red">Virtual</span>
      {:else}
        Preview
      {/if}
    </div>

    <AudioButton
      background={exists}
      disabled={!previewsAvailable || !exists}
      active={thisPlaying}
      style={item.isMusic ? "music" : "sfx"}
      soundState={$soundState}
      soundProgress={$soundProgress}
      on:click={() => playSound(item.hash)}
    />
  </div>

  <!-- Event names -->
  <div class="leading-none" title={item.events.join(", ")}>
    {#if item.events.length > 0}
      {@const event_overflow = item.events.length - 1}
      <span class="relative right-px top-px block text-[8px] uppercase text-zinc-300 opacity-50"> Events: </span>
      <div class="line-clamp-2 max-h-[2lh] overflow-clip text-ellipsis group-hover-focus:text-white">
        <WrappableText text={item.events[0]} />
        {#if event_overflow}
          <span class="text-cyan text-opacity-75 group-hover-focus:text-opacity-100">+{event_overflow}</span>
        {/if}
      </div>
    {/if}
  </div>

  <div class="flex flex-wrap items-start justify-center gap-1 pr-2 leading-none">
    <!-- Visualizer -->
    <div class="flex h-4 w-full flex-row-reverse items-center justify-stretch">
      {#if analyserData && thisPlaying}
        {@const inv128 = 1 / 128}
        {#each $analyserData.values() as item, i (i)}
          {@const value = Math.abs((item - 128) * inv128)}
          <div class="relative min-h-px flex-1 bg-red" style:height="{value ** (value < 0.5 ? 0.5 : 2) * 100}%"></div>
        {/each}
      {/if}
    </div>

    <!-- Info icons -->
    {#if item.location == "bnk"}
      <span class="opacity-75" title="Preloaded in .bnk"><PackageIcon size="0.9x" /></span>
    {:else if item.location == "wem"}
      <span class="opacity-75" title="Separated .wem file"><FileIcon size="0.9x" /></span>
    {/if}

    {#if item.switch?.includes("male")}
      <span class="font-bold text-male" title="Male V">M</span>
    {/if}
    {#if item.switch?.includes("female")}
      <span class="font-bold text-female" title="Female V">F</span>
    {/if}
  </div>

  <!-- Tags -->
  {#if item.tags.length > 0}
    <div class="relative leading-none" title={item.tags.join(", ")}>
      <div class="text-[8px] uppercase text-zinc-300 opacity-50">Event tags:</div>
      <div class="line-clamp-1 w-full text-ellipsis text-sm text-red-dark group-hover-focus:text-red">
        <WrappableText text={item.tags.join(", ")} />
      </div>
    </div>
  {/if}

  <!-- Hash -->
  <div
    class={twMerge(
      "flex border-inherit bg-inherit leading-none",
      "absolute -bottom-2 -right-2 col-span-2 h-[1.5lh] w-[calc(100%-4rem)]",
      "before:absolute before:-left-2 before:bottom-px before:h-2.5 before:w-2.5 before:rotate-45 before:border-b-2 before:border-inherit before:bg-inherit",
      "after:absolute after:-top-2 after:right-px after:h-2.5 after:w-2.5 after:rotate-45 after:border-t-2 after:border-inherit after:bg-inherit",
    )}
  >
    <div class="absolute -inset-[2px] -z-20 border-2 border-inherit"></div>

    <CopyButton
      class="relative z-10 ml-auto block h-6 px-2 py-0 text-sm leading-none opacity-50 transition hover:bg-white hover:bg-opacity-5 hover:!opacity-100 active:opacity-75 group-hover-focus:opacity-75"
      title="Copy hash to clipboard"
      content={item.hash.toString()}
    >
      <span class="pt-1 align-middle uppercase text-zinc-300 text-opacity-75">Hash:</span>
      <span class="inline-block align-middle text-lg leading-none text-cyan">
        {item.hash}
      </span>
    </CopyButton>
  </div>
</a>
