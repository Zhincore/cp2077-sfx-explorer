import type { PageLoad } from "./$types";

export const prerender = "auto";

export const load = (({ params, data }) => {
  return {
    ...data,
    title: params.id ? "Sound Effect " + params.id : "Sound Effects",
    description: "List of all sound effects and music in Cyberpunk 2077.",
  };
}) satisfies PageLoad;
