import { isbotNaive } from "isbot";
import path from "$data/sfx_map.json?url";
import { ITEM_SERVER_RENDER } from "$env/static/private";
import type { PageServerLoad } from "./$types";
import type { SFXMap } from "./sfx";

export const load = (async ({ isDataRequest, request, fetch }) => {
  if (ITEM_SERVER_RENDER !== "true") return;
  if (isDataRequest || !isbotNaive(request.headers.get("user-agent"))) return {};

  // For server render load the data
  return { sfx_map: await fetch(path).then((r) => r.json() as Promise<SFXMap>) };
}) satisfies PageServerLoad;
