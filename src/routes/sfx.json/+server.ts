import { redirect } from '@sveltejs/kit';
import path from "$data/sfx_map.json?url"
import type { RequestHandler } from './$types';

export const prerender = true;

export const GET: RequestHandler = async () => {
    redirect(302, path);
};
