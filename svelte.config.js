import adapter from "@sveltejs/adapter-cloudflare";
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: vitePreprocess(),

  kit: {
    adapter: adapter({
      routes: {
        include: ["/sfx/*", "/subtitles/*"],
      },
    }),
    alias: {
      $lib: "./src/lib",
      $data: "./data",
      $content: "./content",
    },
    paths: {
      relative: false,
    },
  },
};

export default config;
