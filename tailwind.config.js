import defaultConfig from "tailwindcss/defaultConfig";
import plugin from "tailwindcss/plugin";

/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{html,svelte}"],
  theme: {
    extend: {
      colors: ({ colors }) => ({
        zinc: {
          950: "#0f0f0f",
          1000: "#0a0a0a",
        },
        yellow: { DEFAULT: "#f3e600", dark: "#d3c600" },
        cyan: { light: "#75ffff", DEFAULT: "#04daf6" },
        red: { DEFAULT: "#ff003c", dark: "#910022", darker: "#40000f", darkest: "#090002" },
        brown: { DEFAULT: "#554b41" },
        male: { DEFAULT: colors.sky["400"] },
        female: { DEFAULT: colors.pink["400"] },
      }),
      fontFamily: {
        sans: ["Rajdhani", ...defaultConfig.theme.fontFamily.sans],
      },
      dropShadow: {
        glow: "0 0 0.2rem color-mix(in srgb, var(--glow-color, currentColor) 75%, transparent)",
        "glow-strong": "0 0 0.2rem var(--glow-color, currentColor)",
      },
      backdropBlur: {
        xs: "2px",
      },
    },
  },
  plugins: [
    plugin(function ({ addVariant, addUtilities, matchUtilities, addComponents }) {
      addVariant("hover-focus", ["&:hover", "&:focus", "&:focus-within"]);
      addVariant("group-hover-focus", [
        ":merge(.group):hover &",
        ":merge(.group):focus &",
        ":merge(.group):focus-within &",
      ]);
      addVariant("peer-hover-focus", [
        ":merge(.peer):hover ~ &",
        ":merge(.peer):focus ~ &",
        ":merge(.peer):focus-within ~ &",
      ]);

      addUtilities({
        ".bevel-border-2": { "--bevel-border": "2px" },
        ".bevel-border-3": { "--bevel-border": "3px" },
        ".bevel-border-b": { "border-top": "none", "border-bottom": "var(--bevel-border) solid" },
      });
      matchUtilities({
        wh: (value) => ({
          width: value,
          height: value,
        }),
      });
      addComponents({
        ".corner-bevel": {
          "--bevel-border": "2px",
          position: "absolute",
          "pointer-events": "none",
          "border-top": "var(--bevel-border) solid",
          "background-color": "inherit",
          // "clip-path": "polygon(0% 0%, 100% 0%, 50% 75%)",
        },
      });
    }),
  ],
};
