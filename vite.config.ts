import fs from "node:fs/promises";
import { sveltekit } from "@sveltejs/kit/vite";
import { defineConfig } from "vite";

const urlRE = /(\?|&)filesize(?:&|$)/;
const unnededFinalQueryCharRE = /[?&]$/;

export default defineConfig({
  plugins: [
    sveltekit(),
    {
      name: "filesize",
      enforce: "pre",

      async load(id) {
        if (!urlRE.exec(id)) return;

        id = id.replace(urlRE, "$1").replace(unnededFinalQueryCharRE, "");
        const size = (await fs.stat(id)).size;

        if (id.endsWith(".json")) {
          return JSON.stringify(size);
        }
        return `const size = ${size}; export default size;`;
      },
    },
  ],
  server: {
    fs: {
      // Allow serving files from one level up to the project root
      allow: [".."],
    },
  },
});
