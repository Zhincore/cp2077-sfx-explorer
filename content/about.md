# About this

Welcome to my personal project.  
It is very much still a Work-in-Progress, but I hope it's already useful.  
More features and info coming soon!

Data for this website were gathered using my [VoiceSwap scripts](https://github.com/Zhincore/cp2077-voiceswap),
feel free to check that project out too!

**If you encounter any issues or have any suggestions**, feel free to ping me (@Zhincore) on the
[Modding Community Discord](https://discord.gg/Epkq79kd96).  
Alternatively, you can open an issue on the [VoiceSwap's GitHub](https://github.com/Zhincore/cp2077-voiceswap)
or preferably the [Git*Lab* repository of this project](https://gitlab.com/Zhincore/cp2077-sfx-explorer/issues).

Bellow are some guides and information regarding this website.

***You should also checkout the [Cyberpunk 2077 Modding Wiki](https://wiki.redmodding.org/cyberpunk-2077-modding)!***  
There is also a [guide on how to use this site](https://wiki.redmodding.org/cyberpunk-2077-modding/for-mod-creators/modding-guides/sound/finding-an-audio-file).

## Advanced searching

This site uses Fuse.js to search, it provides an *"Extended Search"* feature that you can use to find stuff easier.

By adding some of these specific symbols to your search, the searching behavior changes:

| Symbol | Description | Example |
|---|---|---|
| **=***<\<something>* | Searches the exact event/tag/switch. | `=dev_alarm_04` |
| **'***<\<something>* | Searches items including exact text. | `'maelstrom` |
| **!***<\<something>* | Searches items without text. | `!sex` |
| **^**\\*<\<something>* | Searches items starting with text. | `^mus_` |
| **!^**\\*<\<something>* | Searches items not starting with text. | `!^v_cars` |
| *<\<something>***$** | Searches items ending with text. | `silent$` |
|**!***<\<something>***$** | Searches items not ending with text. | `!enter$` |

For example searching for `^mus_ 'maelstrom` will show  Maelstrom gang music.
Or searching for `'smasher !gun` will find Adam Smasher's sounds (grunts/music/etc) but not his gun sounds.  
If you have a specific hash, you can find it's sound by searching for `=371789278`, if the hash is correct, only that sound should pop up.

## Sound effects (SFX)

In the Sound Effects tab you can search for all the SFX and music I could find in Cyberpunk
using the `eventsmetadata.json` and other files.

For most sounds I provide a low quality **preview** (I compressed all the sounds from 32 GB to approx 600 MB total),  
some sounds are **virtual** (they are marked as 0 bytes in size) and don't have preview,  
some sounds look like they have a preview, but it is **missing** (it'll show a warning icon on play).
Those files I didn't manage to extract *YET*.

If you click a sound in the list, it'll open a **detail window** with more info.
**URL** in the address bar will update too, so you can share specific sounds with others.  
In that window you can **copy** the hash, event names or tags by clicking on them, you can then paste it elsewhere using Ctrl+V.  
In the list view, only hashes are copyable.

### SFX Explanation

A lot of sounds are tied to more than one **events** and one event can have multiple sounds
that are either picked randomly, picked according to some conditions or layered over each other.
Event is simply a moment or a situation in the game that plays sounds.

Every event also has **tags**, tags from all events of a sound are accumulated together in the sound's info on this site.

Some sounds require even more specific situations to play, for example the gender of V,
stamina exhaustion, car speed, etc. These sounds have **switches**, **game parameters** or **states**
listed in the details of the sound. Those can be used for searching.

#### SFX Locations

All of the sounds are contained in the game's archives, so you need [WolvenKit](https://redmodding.org/) to get to them.  
Sounds are identified by their **hashes**, which you can find on this website.

Most of the SFX are additionally packed in **`.opuspak`** files and can extracted using tools such as
[SoundFX Search and Update](https://www.nexusmods.com/cyberpunk2077/mods/11075).
This website can tell you the name of the opuspak, but the hash or event name should suffice.

Music is mostly in a separate **`.wem`** file and WolvenKit should be able to export it.
The name of the files is simply `<hash>.wem`.

Few of the sounds are embedded directly in **`.bnk`** files that describe how and when to play all the sounds,
but the smaller and frequently needed sounds are *"preloaded"* in them.
For those you probably need more advanced methods like using [vgmstream](https://vgmstream.org/), but it may fail at the moment.

Other sounds are 0 bytes in size, those I marked as **virtual** or **nonexistent**. Those sounds are usually either generated (e.g. a sine wave)
or taken from other sources than an audio file. Some of them may be falsely marked as such and may be accessible somehow,
but that requires more research.

## Voiceover and Subtitles

**THIS SECTION IS STILL WORK-IN-PROGRESS**

The Voiceover and Subtitles tab offers a list of all of the subtitles and their voiceovers I managed to extract from the game.

Not all voiceovers have subtitles and not all subtitles have voiceovers.
Some voiceovers have missing subtitles for some reason, I plan to subtitle them manually in the future.
Some subtitles are **actions** that can be chosen by the player or other text that is not spoken.

As for SFX, for voiceovers I also provide low-quality **previews**.
Non-normal (holocall/helmet) voiceovers are currently missing, I might add them in the future.

The game uses `voiceover*.json` files to store mappings between a numeric ID and paths to the voiceover audio.
Then there are a few thousands of json files containing the subtitles for those.

## Raw data (JSON API)

You can get the raw JSON data this page uses by appending `.json` to the section's URL
and you will be redirected to the file.

Specifically, to get the JSON of:

- **SFX** go to [/sfx.json](/sfx.json) *([TypeScript definition](https://gitlab.com/Zhincore/cp2077-sfx-explorer/-/blob/master/src/routes/sfx/%5B%5Bid%5D%5D/sfx.ts))*
- **voiceovers and subtitles** go to [/subtitles.json](/subtitles.json) *([TypeScript definition](https://gitlab.com/Zhincore/cp2077-sfx-explorer/-/blob/master/src/routes/subtitles/%5B%5Bid%5D%5D/subtitles.ts))*

You can check the last update on [/version.json](/version.json).

**NOTE:** The format of the JSON files might change in the future.
The TypeScript definitions that I use are linked above too.
You can use them to get an idea of what is in the JSONs.
