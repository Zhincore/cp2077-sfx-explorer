# Cyberpunk 2077 Sound Explorer

> Website that allows you to search and explore SFX and subtitles + voiceovers of Cyberpunk 2077.

Data for this project were gathered using my [VoiceSwap scripts](https://github.com/Zhincore/cp2077-voiceswap).

**If you encounter any issues or have any suggestions**, feel free to ping me (@Zhincore) on the
[Modding Community Discord](https://discord.gg/Epkq79kd96) or [my own Discord server](https://discord.gg/5mVrUh34Nd).  
Alternatively, you can open an issue on the [VoiceSwap's GitHub](https://github.com/Zhincore/cp2077-voiceswap)
or preferably the [Git*Lab* repository of this project](https://gitlab.com/Zhincore/cp2077-sfx-explorer/issues).

![screenshot](screenshot.png)

## TODO

Here are some of the features I plan to add, feel free to suggest more at the link above!

- Fix messy fancy slanted corners
- Bookmarking items ?
- Add a list of nodes that lead from an event to sound
